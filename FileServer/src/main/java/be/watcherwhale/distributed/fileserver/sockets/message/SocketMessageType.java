package be.watcherwhale.distributed.fileserver.sockets.message;

public enum SocketMessageType { ANNOUNCE, DOWNLOAD, RECEIVED }
