package be.watcherwhale.distributed.fileserver.sockets;

import be.watcherwhale.distributed.fileserver.FileServer;
import be.watcherwhale.distributed.fileserver.sockets.message.SocketMessage;
import be.watcherwhale.distributed.fileserver.sockets.message.SocketMessageBuilder;
import be.watcherwhale.distributed.fileserver.sockets.message.SocketMessageType;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

public class SocketServer
{
    private DatagramSocket serverSocket;
    private boolean running;
    private final int port;
    private byte[] buf = new byte[256];

    private SocketServer(int port) throws SocketException
    {
        this.port = port;
        this.serverSocket = new DatagramSocket(this.port);
    }

    private void startServer()
    {
        this.running = true;

        while(this.running)
        {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);

            try
            {
                this.serverSocket.receive(packet);
            }
            catch(Exception ex)
            {
                continue;
            }

            InetAddress ip = packet.getAddress();
            int port = packet.getPort();

            String received = new String(packet.getData(), 0, packet.getLength());
            try
            {
                SocketMessage msg = SocketMessage.parse(received);
                if(msg.getType() == SocketMessageType.ANNOUNCE)
                    msg.populate();

                if(msg.getType() != SocketMessageType.RECEIVED)
                    this.sendReceived(ip, port);

            }
            catch (Exception ignored)
            {

            }
        }

        this.serverSocket.close();
    }

    public void sendReceived(InetAddress ip, int port) throws IOException
    {
        var responseBuilder = new SocketMessageBuilder();
        var response = responseBuilder.node(FileServer.node)
                .type(SocketMessageType.RECEIVED)
                .build();

        this.sendMessage(ip, port, response);
    }

    public void sendMessage(InetAddress address, int port, SocketMessage msg) throws IOException
    {
        DatagramSocket socket = new DatagramSocket();
        byte[] buf = msg.toString().getBytes(StandardCharsets.UTF_8);
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
        socket.send(packet);
        socket.close();
    }

    public void stop()
    {
        this.running = false;
    }

    public static SocketServer start() throws SocketException
    {
        SocketServer server = new SocketServer(6666);
        server.startServer();

        return server;
    }
}
