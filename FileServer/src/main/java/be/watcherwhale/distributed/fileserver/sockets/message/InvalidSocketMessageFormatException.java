package be.watcherwhale.distributed.fileserver.sockets.message;

public class InvalidSocketMessageFormatException extends Exception
{
    public InvalidSocketMessageFormatException(String errorMessage)
    {
        super(errorMessage);
    }
}
