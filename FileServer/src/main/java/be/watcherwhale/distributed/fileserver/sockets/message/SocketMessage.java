package be.watcherwhale.distributed.fileserver.sockets.message;

import be.watcherwhale.distributed.utils.NodeInfo;
import be.watcherwhale.distributed.utils.Requester;
import be.watcherwhale.distributed.utils.bodies.NodeBody;

import java.io.IOException;

public class SocketMessage
{
    private String raw;
    private NodeInfo node;
    private SocketMessageType type;
    private String arg;

    SocketMessage() {}

    private SocketMessage(String raw)
    {
        this.raw = raw;
    }

    private void parse() throws InvalidSocketMessageFormatException
    {
        var msgSegments = this.raw.split(" ");

        if(!msgSegments[0].equals("NODE"))
        {
            throw new InvalidSocketMessageFormatException("The message doesn't start with Node identifier.");
        }

        this.node = new NodeInfo();
        this.node.setName(msgSegments[1]);

        this.type = SocketMessageType.valueOf(msgSegments[2]);
        this.arg = msgSegments[3];
    }

    /**
     * Populate the node info
     */
    public void populate() throws IOException, InterruptedException
    {
        NodeBody response = (NodeBody) Requester.doGetRequest("http://localhost:9000/node/whois/" +
                this.node.getName(), NodeBody.class);

        this.node = NodeInfo.buildFromBody(response);
    }

    void setNode(NodeInfo node)
    {
        this.node = node;
    }

    void setType(SocketMessageType type)
    {
        this.type = type;
    }

    void setArg(String arg)
    {
        this.arg = arg;
    }

    void buildRaw()
    {
        this.raw = "NODE " + this.node.getName() + " " + this.type.name();

        if(this.arg != null && !this.arg.isEmpty())
            this.raw += " " + this.arg;
    }

    public SocketMessageType getType()
    {
        return type;
    }

    @Override
    public String toString()
    {
        return this.raw;
    }

    public static SocketMessage parse(String raw) throws InvalidSocketMessageFormatException
    {
        var msg = new SocketMessage(raw);
        msg.parse();

        return msg;
    }

    public static SocketMessage parse(String raw, boolean populate) throws Exception
    {
        var msg = SocketMessage.parse(raw);
        if(populate) msg.populate();

        return msg;
    }
}

