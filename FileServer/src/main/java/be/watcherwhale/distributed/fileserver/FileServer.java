package be.watcherwhale.distributed.fileserver;

import be.watcherwhale.distributed.fileserver.sockets.SocketServer;
import be.watcherwhale.distributed.utils.NodeInfo;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.net.SocketException;

@SpringBootApplication
public class FileServer
{
    public static NodeInfo node;
    public static SocketServer server;

    public static void main(String[] args) throws SocketException
    {
        new SpringApplicationBuilder(FileServer.class)
                .web(WebApplicationType.NONE)
                .run(args);

        node = NodeInfo.fromFile("node.json");
        server = SocketServer.start();
    }
}
