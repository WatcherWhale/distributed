package be.watcherwhale.distributed.fileserver.sockets.message;

import be.watcherwhale.distributed.utils.NodeInfo;

public class SocketMessageBuilder
{
    private NodeInfo node;
    private SocketMessageType type;
    private String msgArg = "";

    public SocketMessageBuilder node(NodeInfo node)
    {
        this.node = node;
        return this;
    }

    public SocketMessageBuilder type(SocketMessageType type)
    {
        this.type = type;
        return this;
    }

    public SocketMessageBuilder arg(String arg)
    {
        this.msgArg = arg;
        return this;
    }

    public SocketMessage build()
    {
        SocketMessage msg = new SocketMessage();
        msg.setNode(this.node);
        msg.setType(this.type);
        msg.setArg(this.msgArg);
        msg.buildRaw();

        return msg;
    }
}
