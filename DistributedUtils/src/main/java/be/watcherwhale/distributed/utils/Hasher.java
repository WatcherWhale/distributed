package be.watcherwhale.distributed.utils;

public class Hasher
{
    public static int hash(Object o)
    {
        int baseHash = o.hashCode();

        // Calculate sum of the 2 bit groups
        int hash = (baseHash & 0xFFFF) + ((baseHash >> 16) & 0xFFFF);

        // Invert bits if 16th bit is set high
        if((hash & 0x8000) == 0x8000)
        {
            hash = hash ^ 0xFFFF;
        }

        // Make sure 15 bits are returned
        return hash & 0x7FFF;
    }
}
