package be.watcherwhale.distributed.utils;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Requester
{
    private static final Gson gson = new Gson();

    public static String toJson(Object o)
    {
        return gson.toJson(o);
    }

    public static void doGetRequest(String url) throws IOException, InterruptedException
    {
        var request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .GET()
                .build();

        var client = HttpClient.newHttpClient();
        client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    public static Object doGetRequest(String url, Class c) throws IOException, InterruptedException
    {
        var request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .GET()
                .build();

        var client = HttpClient.newHttpClient();
        var response = client.send(request, HttpResponse.BodyHandlers.ofString());

        if(response.statusCode() == 200)
        {
            return gson.fromJson(response.body(), c);
        }

        return null;
    }

    public static void doPostRequest(String url, String body) throws IOException, InterruptedException
    {
        var request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();

        var client = HttpClient.newHttpClient();
        client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    public static Object doPostRequest(String url, String body, Class c) throws IOException, InterruptedException
    {
        var request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();

        var client = HttpClient.newHttpClient();
        var response = client.send(request, HttpResponse.BodyHandlers.ofString());

        if(response.statusCode() == 200)
        {
            return gson.fromJson(response.body(), c);
        }

        return null;
    }
}
