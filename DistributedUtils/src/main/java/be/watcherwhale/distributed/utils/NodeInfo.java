package be.watcherwhale.distributed.utils;

import be.watcherwhale.distributed.utils.bodies.NodeBody;
import be.watcherwhale.distributed.utils.bodies.PublishBody;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

public class NodeInfo implements java.io.Serializable
{
    private String name;
    private int hash;
    private String ip;
    private int port;
    private ArrayList<String> files;

    public NodeInfo()
    {
        this.files = new ArrayList<>();
    }

    public NodeInfo(String name, String ip, int port)
    {
        this.name = name;
        this.ip = ip;
        this.port = port;
        this.files = new ArrayList<>();

        this.hash = Hasher.hash(this.name);
    }

    public void setName(String name)
    {
        this.name = name;
        this.recalculateHash();
    }

    public void setHash(int hash)
    {
        this.hash = hash;
    }

    private void recalculateHash()
    {
        this.hash = Hasher.hash(this.name);
    }

    public void setFiles(ArrayList<String> files)
    {
        this.files = files;
    }

    public String getName()
    {
        return name;
    }

    public int getHash()
    {
        return hash;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public void addFiles(Collection<String> files)
    {
        this.files.addAll(files);
    }

    public void addFile(String file)
    {
        this.files.add(file);
    }

    public ArrayList<String> getFiles()
    {
        return this.files;
    }

    public static NodeInfo buildFromBody(PublishBody body)
    {
        NodeInfo node = new NodeInfo(body.getName(), body.getIpAddress(), body.getPort());
        node.addFiles(body.getFiles());
        return node;
    }

    public static NodeInfo buildFromBody(NodeBody body)
    {
        return new NodeInfo(body.getName(), body.getIp(), body.getPort());
    }

    public static NodeInfo fromFile(String filename)
    {
        try
        {
            var jsonFile = new File(filename);
            var scanner = new Scanner(jsonFile);
            StringBuilder json = new StringBuilder();
            while (scanner.hasNextLine())
            {
                json.append(scanner.nextLine()).append("\n");
            }

            var node = new Gson().fromJson(json.toString(), NodeInfo.class);
            node.recalculateHash();

            return node;
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
