package be.watcherwhale.distributed.utils.bodies;

import be.watcherwhale.distributed.utils.NodeInfo;

public class NodeBody implements java.io.Serializable
{
    private String name;
    private String ip;
    private int port;

    public NodeBody() { }

    public NodeBody(String name, String ip, int port)
    {
        this.name = name;
        this.ip = ip;
        this.port = port;
    }

    public NodeBody(NodeInfo info)
    {
        this.name = info.getName();
        this.ip = info.getIp();
        this.port = info.getPort();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }
}
