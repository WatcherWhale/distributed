package be.watcherwhale.distributed.utils.bodies;

import java.util.ArrayList;

public class PublishBody implements java.io.Serializable
{
    private String name;
    private String ipAddress;
    private int port;
    private ArrayList<String> files;

    public PublishBody() { }

    public PublishBody(String name, String ipAddress, int port, ArrayList<String> fileList)
    {
        this.name = name;
        this.ipAddress = ipAddress;
        this.files = fileList;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getIpAddress()
    {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public ArrayList<String> getFiles()
    {
        return files;
    }

    public void setFiles(ArrayList<String> files)
    {
        this.files = files;
    }
}
