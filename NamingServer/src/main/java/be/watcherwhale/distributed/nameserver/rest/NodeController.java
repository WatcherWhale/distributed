package be.watcherwhale.distributed.nameserver.rest;

import be.watcherwhale.distributed.nameserver.Database;
import be.watcherwhale.distributed.utils.Hasher;
import be.watcherwhale.distributed.utils.NodeInfo;
import be.watcherwhale.distributed.utils.bodies.PublishBody;
import be.watcherwhale.distributed.utils.bodies.NodeBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NodeController
{
    private static final Logger logger = LoggerFactory.getLogger(NodeController.class);

    @PostMapping("/nodes/publish")
    public void publish(@RequestBody PublishBody body)
    {
        logger.info("Node '" + body.getName() + "' with ip '" + body.getIpAddress() + "' published itself.");

        Database.getInstance().addNode(NodeInfo.buildFromBody(body));
    }

    @PostMapping("/nodes/suppress")
    public void suppress(@RequestBody NodeBody body)
    {
        logger.info("Node '" + body.getName() + "' is suppressing itself.");

        Database.getInstance().removeNode(NodeInfo.buildFromBody(body));
    }

    @GetMapping("/nodes/whois/{node}")
    public NodeBody whois(String hostname)
    {
        return Database.getInstance().getNodeByName(hostname);
    }
}
