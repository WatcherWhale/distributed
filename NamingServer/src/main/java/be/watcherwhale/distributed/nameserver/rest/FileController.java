package be.watcherwhale.distributed.nameserver.rest;

import be.watcherwhale.distributed.nameserver.Database;
import be.watcherwhale.distributed.utils.bodies.NodeBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class FileController
{
    @GetMapping("/file/{filename}")
    public ArrayList<NodeBody> findFile(@PathVariable String filename)
    {
        return Database.getInstance().findFile(filename);
    }
}
