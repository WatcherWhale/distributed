package be.watcherwhale.distributed.nameserver;

import be.watcherwhale.distributed.utils.Hasher;
import be.watcherwhale.distributed.utils.NodeInfo;
import be.watcherwhale.distributed.utils.bodies.NodeBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Database
{
    private static final Logger logger = LoggerFactory.getLogger(Database.class);

    private HashMap<Integer, NodeInfo> nodes;
    private HashMap<Integer, Integer> fileList;

    private Database()
    {
        this.nodes = new HashMap<>();
        this.fileList = new HashMap<>();
    }

    private Database(HashMap<Integer, NodeInfo> nodes)
    {
        this.nodes = nodes;
        this.fileList = new HashMap<>();

        // Build file cache
        this.rebuildFileList();
    }

    private void rebuildFileList()
    {
        for(NodeInfo node : this.nodes.values())
        {
            for(String file : node.getFiles())
            {
                int fileHash = Hasher.hash(file);
                this.fileList.put(fileHash, node.getHash());
            }
        }
    }

    public void addNode(NodeInfo node)
    {
        this.nodes.put(node.getHash(), node);
        this.rebuildFileList();

        this.save();
    }

    public void removeNode(NodeInfo info)
    {
        NodeInfo node = this.nodes.get(info.getHash());
        if(node == null)
        {
            logger.warn("Tried deleting non-existing node.");
            return;
        }

        if(node.getName().equals(info.getName()) && node.getIp().equals(info.getIp()) && node.getPort() == info.getPort())
        {
            this.nodes.remove(info.getHash());
            logger.info("Removed node '" + info.getName() + "'.");
            this.save();
        }
        else
        {
            logger.warn("Tried to delete an invalid node.");
        }
    }

    public NodeInfo getNode(int hash)
    {
        return this.nodes.get(hash);
    }

    public ArrayList<NodeBody> findFile(String filename)
    {
        ArrayList<NodeBody> nodes = new ArrayList<>();

        int fileHash = Hasher.hash(filename);
        NodeInfo hostNode = this.nodes.get(this.fileList.get(fileHash));

        if(hostNode.getFiles().contains(filename))
        {
            nodes.add(new NodeBody(hostNode));
        }

        NodeInfo replicate = this.findReplicate(fileHash);
        if(replicate != hostNode)
            nodes.add(new NodeBody(replicate));

        return nodes;
    }

    public NodeBody getNodeByName(String name)
    {
        return new NodeBody(this.getNode(Hasher.hash(name)));
    }

    private NodeInfo findReplicate(int fileHash)
    {
        ArrayList<Integer> keys = (ArrayList<Integer>) this.nodes.keySet().stream().sorted((a, b) -> b - a).collect(Collectors.toList());

        for (int nodeHash : keys)
        {
            if(nodeHash < fileHash)
                return this.nodes.get(nodeHash);
        }

        return this.nodes.get(keys.get(0));
    }

    @PreDestroy
    public void save()
    {
        File databaseFile = new File("database.xml");

        try
        {
            if (!databaseFile.exists())
            {
                databaseFile.createNewFile();
            }

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            XMLEncoder enc = new XMLEncoder(os);
            enc.writeObject(this.nodes);
            enc.close();

            FileWriter writer = new FileWriter(databaseFile);
            writer.write(os.toString());

            writer.close();
            os.close();

            logger.info("Database has been saved to the local file system.");
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage());
        }
    }

    public static void load()
    {
        File databaseFile = new File("database.xml");
        if(!databaseFile.exists())
        {
            logger.info("No database found, using empty database.");
            Database._db = new Database();
            return;
        }

        try
        {
            FileInputStream fi = new FileInputStream(databaseFile);
            XMLDecoder dec = new XMLDecoder(fi);

            Database._db = new Database((HashMap<Integer, NodeInfo>) dec.readObject());
            dec.close();
            fi.close();
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage());
            logger.info("Using empty database.");
            Database._db = new Database();
        }
    }

    private static Database _db;
    public static Database getInstance()
    {
        return Database._db;
    }
}
