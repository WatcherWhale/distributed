package be.watcherwhale.distributed.nameserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NameServer
{
    private static final Logger logger = LoggerFactory.getLogger(NameServer.class);

    public static void main(String[] args)
    {
        SpringApplication.run(NameServer.class, args);

        logger.info("Loading database.");
        Database.load();
        logger.info("Database loaded.");
    }
}
